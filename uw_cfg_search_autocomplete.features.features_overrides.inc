<?php

/**
 * @file
 * uw_cfg_search_autocomplete.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_search_autocomplete_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the
  // magic.
  $overrides = array();

  // Exported overrides for: views_view.
  $overrides["views_view.nodes_autocomplete.argument"] = NULL;
  $overrides["views_view.nodes_autocomplete.base_database"] = NULL;
  $overrides["views_view.nodes_autocomplete.current_page"] = NULL;
  $overrides["views_view.nodes_autocomplete.display|default|display_options|exposed_form|options|exposed_sorts_label"]["DELETED"] = TRUE;
  $overrides["views_view.nodes_autocomplete.empty"] = NULL;
  $overrides["views_view.nodes_autocomplete.field"] = NULL;
  $overrides["views_view.nodes_autocomplete.filter"] = NULL;
  $overrides["views_view.nodes_autocomplete.footer"] = NULL;
  $overrides["views_view.nodes_autocomplete.header"] = NULL;
  $overrides["views_view.nodes_autocomplete.is_attachment"] = NULL;
  $overrides["views_view.nodes_autocomplete.items_per_page"] = NULL;
  $overrides["views_view.nodes_autocomplete.offset"] = NULL;
  $overrides["views_view.nodes_autocomplete.override_path"] = NULL;
  $overrides["views_view.nodes_autocomplete.override_url"] = NULL;
  $overrides["views_view.nodes_autocomplete.relationship"] = NULL;
  $overrides["views_view.nodes_autocomplete.row_index"] = NULL;
  $overrides["views_view.nodes_autocomplete.sort"] = NULL;
  $overrides["views_view.nodes_autocomplete.style_options"] = NULL;
  $overrides["views_view.nodes_autocomplete.style_plugin"] = NULL;
  $overrides["views_view.nodes_autocomplete.total_rows"] = NULL;
  $overrides["views_view.users_autocomplete.argument"] = NULL;
  $overrides["views_view.users_autocomplete.base_database"] = NULL;
  $overrides["views_view.users_autocomplete.current_page"] = NULL;
  $overrides["views_view.users_autocomplete.display|default|display_options|exposed_form|options|exposed_sorts_label"]["DELETED"] = TRUE;
  $overrides["views_view.users_autocomplete.empty"] = NULL;
  $overrides["views_view.users_autocomplete.field"] = NULL;
  $overrides["views_view.users_autocomplete.filter"] = NULL;
  $overrides["views_view.users_autocomplete.footer"] = NULL;
  $overrides["views_view.users_autocomplete.header"] = NULL;
  $overrides["views_view.users_autocomplete.is_attachment"] = NULL;
  $overrides["views_view.users_autocomplete.items_per_page"] = NULL;
  $overrides["views_view.users_autocomplete.offset"] = NULL;
  $overrides["views_view.users_autocomplete.override_path"] = NULL;
  $overrides["views_view.users_autocomplete.override_url"] = NULL;
  $overrides["views_view.users_autocomplete.relationship"] = NULL;
  $overrides["views_view.users_autocomplete.row_index"] = NULL;
  $overrides["views_view.users_autocomplete.sort"] = NULL;
  $overrides["views_view.users_autocomplete.style_options"] = NULL;
  $overrides["views_view.users_autocomplete.style_plugin"] = NULL;
  $overrides["views_view.users_autocomplete.total_rows"] = NULL;
  $overrides["views_view.words_autocomplete.argument"] = NULL;
  $overrides["views_view.words_autocomplete.base_database"] = NULL;
  $overrides["views_view.words_autocomplete.current_page"] = NULL;
  $overrides["views_view.words_autocomplete.display|default|display_options|exposed_form|options|exposed_sorts_label"]["DELETED"] = TRUE;
  $overrides["views_view.words_autocomplete.empty"] = NULL;
  $overrides["views_view.words_autocomplete.field"] = NULL;
  $overrides["views_view.words_autocomplete.filter"] = NULL;
  $overrides["views_view.words_autocomplete.footer"] = NULL;
  $overrides["views_view.words_autocomplete.header"] = NULL;
  $overrides["views_view.words_autocomplete.is_attachment"] = NULL;
  $overrides["views_view.words_autocomplete.items_per_page"] = NULL;
  $overrides["views_view.words_autocomplete.offset"] = NULL;
  $overrides["views_view.words_autocomplete.override_path"] = NULL;
  $overrides["views_view.words_autocomplete.override_url"] = NULL;
  $overrides["views_view.words_autocomplete.relationship"] = NULL;
  $overrides["views_view.words_autocomplete.row_index"] = NULL;
  $overrides["views_view.words_autocomplete.sort"] = NULL;
  $overrides["views_view.words_autocomplete.style_options"] = NULL;
  $overrides["views_view.words_autocomplete.style_plugin"] = NULL;
  $overrides["views_view.words_autocomplete.total_rows"] = NULL;

  return $overrides;
}
