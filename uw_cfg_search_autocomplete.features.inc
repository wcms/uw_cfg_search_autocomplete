<?php

/**
 * @file
 * uw_cfg_search_autocomplete.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_search_autocomplete_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_cfg_search_autocomplete_views_default_views_alter(&$data) {
  if (isset($data['nodes_autocomplete'])) {
    $data['nodes_autocomplete']->argument = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->base_database = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->current_page = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->empty = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->field = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->filter = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->footer = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->header = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->is_attachment = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->items_per_page = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->offset = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->override_path = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->override_url = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->relationship = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->row_index = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->sort = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->style_options = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->style_plugin = NULL; /* WAS: '' */
    $data['nodes_autocomplete']->total_rows = NULL; /* WAS: '' */
    unset($data['nodes_autocomplete']->display['default']->display_options['exposed_form']['options']['exposed_sorts_label']);
  }
  if (isset($data['users_autocomplete'])) {
    $data['users_autocomplete']->argument = NULL; /* WAS: '' */
    $data['users_autocomplete']->base_database = NULL; /* WAS: '' */
    $data['users_autocomplete']->current_page = NULL; /* WAS: '' */
    $data['users_autocomplete']->empty = NULL; /* WAS: '' */
    $data['users_autocomplete']->field = NULL; /* WAS: '' */
    $data['users_autocomplete']->filter = NULL; /* WAS: '' */
    $data['users_autocomplete']->footer = NULL; /* WAS: '' */
    $data['users_autocomplete']->header = NULL; /* WAS: '' */
    $data['users_autocomplete']->is_attachment = NULL; /* WAS: '' */
    $data['users_autocomplete']->items_per_page = NULL; /* WAS: '' */
    $data['users_autocomplete']->offset = NULL; /* WAS: '' */
    $data['users_autocomplete']->override_path = NULL; /* WAS: '' */
    $data['users_autocomplete']->override_url = NULL; /* WAS: '' */
    $data['users_autocomplete']->relationship = NULL; /* WAS: '' */
    $data['users_autocomplete']->row_index = NULL; /* WAS: '' */
    $data['users_autocomplete']->sort = NULL; /* WAS: '' */
    $data['users_autocomplete']->style_options = NULL; /* WAS: '' */
    $data['users_autocomplete']->style_plugin = NULL; /* WAS: '' */
    $data['users_autocomplete']->total_rows = NULL; /* WAS: '' */
    unset($data['users_autocomplete']->display['default']->display_options['exposed_form']['options']['exposed_sorts_label']);
  }
  if (isset($data['words_autocomplete'])) {
    $data['words_autocomplete']->argument = NULL; /* WAS: '' */
    $data['words_autocomplete']->base_database = NULL; /* WAS: '' */
    $data['words_autocomplete']->current_page = NULL; /* WAS: '' */
    $data['words_autocomplete']->empty = NULL; /* WAS: '' */
    $data['words_autocomplete']->field = NULL; /* WAS: '' */
    $data['words_autocomplete']->filter = NULL; /* WAS: '' */
    $data['words_autocomplete']->footer = NULL; /* WAS: '' */
    $data['words_autocomplete']->header = NULL; /* WAS: '' */
    $data['words_autocomplete']->is_attachment = NULL; /* WAS: '' */
    $data['words_autocomplete']->items_per_page = NULL; /* WAS: '' */
    $data['words_autocomplete']->offset = NULL; /* WAS: '' */
    $data['words_autocomplete']->override_path = NULL; /* WAS: '' */
    $data['words_autocomplete']->override_url = NULL; /* WAS: '' */
    $data['words_autocomplete']->relationship = NULL; /* WAS: '' */
    $data['words_autocomplete']->row_index = NULL; /* WAS: '' */
    $data['words_autocomplete']->sort = NULL; /* WAS: '' */
    $data['words_autocomplete']->style_options = NULL; /* WAS: '' */
    $data['words_autocomplete']->style_plugin = NULL; /* WAS: '' */
    $data['words_autocomplete']->total_rows = NULL; /* WAS: '' */
    unset($data['words_autocomplete']->display['default']->display_options['exposed_form']['options']['exposed_sorts_label']);
  }
}

/**
 * Implements hook_search_autocomplete_config_features_default_settings().
 */
function uw_cfg_search_autocomplete_search_autocomplete_config_features_default_settings() {
  return array(
    0 => array(
      'fid' => 1,
      'title' => 'Search page - Node Tab  (search/node/%)',
      'selector' => '#search-form[action="/search/node"] #edit-keys',
      'weight' => 0,
      'enabled' => 0,
      'parent_fid' => 0,
      'min_char' => 3,
      'max_sug' => 10,
      'no_results' => '{"label":"No results found for [search-phrase]. Click to perform full search.","value":"[search-phrase]","link":"","group":{"group_id":"no_results"}}',
      'all_results' => '{"label":"View all results for [search-phrase].","value":"[search-phrase]","link":"","group":{"group_id":"all_results"}}',
      'auto_submit' => 1,
      'auto_redirect' => 1,
      'translite' => 1,
      'data_source' => 'view',
      'data_callback' => 'search_autocomplete/autocomplete/1/',
      'data_static' => '',
      'data_view' => 'words_autocomplete',
      'theme' => 'basic-green.css',
    ),
    1 => array(
      'fid' => 2,
      'title' => 'Search page - User Tab  (search/user/%)',
      'selector' => '#search-form[action="/search/user"] #edit-keys',
      'weight' => 1,
      'enabled' => 0,
      'parent_fid' => 0,
      'min_char' => 3,
      'max_sug' => 10,
      'no_results' => '{"label":"No results found for [search-phrase]. Click to perform full search.","value":"[search-phrase]","link":"","group":{"group_id":"no_results"}}',
      'all_results' => '{"label":"View all results for [search-phrase].","value":"[search-phrase]","link":"","group":{"group_id":"all_results"}}',
      'auto_submit' => 1,
      'auto_redirect' => 1,
      'translite' => 1,
      'data_source' => 'view',
      'data_callback' => 'search_autocomplete/autocomplete/2/',
      'data_static' => '',
      'data_view' => 'users_autocomplete',
      'theme' => 'user-blue.css',
    ),
    2 => array(
      'fid' => 3,
      'title' => 'Search Block',
      'selector' => '#edit-search-block-form--2',
      'weight' => 0,
      'enabled' => 0,
      'parent_fid' => 0,
      'min_char' => 3,
      'max_sug' => 10,
      'no_results' => '{"label":"No results found for [search-phrase]. Click to perform full search.","value":"[search-phrase]","link":"","group":{"group_id":"no_results"}}',
      'all_results' => '{"label":"View all results for [search-phrase].","value":"[search-phrase]","link":"","group":{"group_id":"all_results"}}',
      'auto_submit' => 1,
      'auto_redirect' => 1,
      'translite' => 1,
      'data_source' => 'view',
      'data_callback' => 'search_autocomplete/autocomplete/3/',
      'data_static' => '',
      'data_view' => 'nodes_autocomplete',
      'theme' => 'basic-green.css',
    ),
  );
}
